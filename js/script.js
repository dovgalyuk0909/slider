class Main{
	constructor(){
		this.$butNext = document.querySelector('.next');
		this.$butPrev = document.querySelector('.prev');
		this.$container = document.querySelector('.main-block');
		this.$rightGallery = document.querySelector('.wrappList');
		this.widthImg = this.$container.clientWidth;
		this.widthCount = 0;
		this.indexEl = 0;
		this.countIn = 0;
		this.$butNext.addEventListener('click',this.nextSlide.bind(this));
		this.$butPrev.addEventListener('click',this.prevSlide.bind(this));
		this.$rightGallery.addEventListener('click',this.clickRightGalery.bind(this));


	}
	drawListImg(){
		 let arrimg = [];
		for(let i=1;i<=this.$container.children.length;i++){
			let photo = document.createElement('img');
			photo.setAttribute('src',`icon/foto${i}.jpg`);
			arrimg.push(photo);
			
		}
		for(let j=0;j<arrimg.length;j++){
			this.$rightGallery.appendChild(arrimg[j]);
		}
	}
	maxWidth(){
		let maxsize = 0;
		for(let i=0;i<this.$container.children.length;i++){
			maxsize = maxsize + this.$container.children[i].clientWidth;
		}
		return maxsize;
	}
	nextSlide(){
		if(this.widthCount < this.maxWidth() - this.widthImg){
			this.$container.style.transition= "transform 1s";
			this.widthCount = this.widthCount + this.widthImg;
			this.$container.style.transform = `translateX(-${this.widthCount}px)`;

		}else{
			return
		}
			this.$rightGallery.children[this.countIn].style.border = "0";
			this.countIn++;
			this.$rightGallery.children[this.countIn].style.border = "2px solid #1f1f69";
	}
	prevSlide(){
		if(this.widthCount > 0){
			this.$container.style.transition= "transform 1s";
			this.$container.style.transform = `translateX(-${this.widthCount - this.widthImg}px`;
			this.widthCount = this.widthCount - this.widthImg;
		}else{
			return
		}
			this.$rightGallery.children[this.countIn].style.border = "0";
			this.countIn--;
			this.$rightGallery.children[this.countIn].style.border = "2px solid #1f1f69";

	}
	clickRightGalery(event){
		let clickField = event.target,
		htmlEl = clickField.outerHTML;
		this.widthCount = 0;
		if(clickField.tagName === "IMG"){

			for(let i=0;i<this.$rightGallery.children.length;i++){
				if(this.$rightGallery.children[i].outerHTML === htmlEl){
					this.indexEl = i;
					this.$rightGallery.children[this.countIn].style.border = "0";
					this.countIn = i;
					this.$rightGallery.children[this.countIn].style.border = "2px solid #1f1f69";
					break;
				}
			}
			for(let j=0;j<this.indexEl;j++){
				this.widthCount = this.widthCount + this.widthImg;
			}
			this.$container.style.transition= "transform 0s";
			this.$container.style.transform = `translateX(-${this.widthCount}px`;
		}else{
			return
		}
	}
}
document.addEventListener('DOMContentLoaded',()=> {
	 	this.main = new Main();
	 	main.drawListImg();
});













